# Solid Rust URL's worth using

# Math, Graphics, Physics, gaming libraries

* [https://arewegameyet.rs/](https://arewegameyet.rs/)
* [https://github.com/dimforge](https://github.com/dimforge)
* [https://nalgebra.org/](https://nalgebra.org/)
* [https://rapier.rs/](https://rapier.rs/)
* [https://parry.rs/](https://parry.rs/)
* [https://salva.rs/](https://salva.rs/)

## Game Engines

* [https://amethyst.rs/](https://amethyst.rs/)
* [https://book.amethyst.rs/book/stable/](https://book.amethyst.rs/book/stable/)



